/*=============================================================================
Name: 		Eduardo Ortiz
e-mail:		eduardo3991@hotmail.com
=============================================================================*/
#include <iostream>
#include "SweetArray.h"
using namespace std;

#ifndef LINKLIST
#define LINKLIST

//The Nodes used on the class LinkList
class Node
{
	public:

		int element;
		Node* next;

		Node(int a = 0, Node* p = NULL)
		{
			element = a;
			next = p;
		}

		void printNode(ostream& out)
		{
			out << dec << element << "{" << hex << this << "}";
		}
};

//Linked List class from one direction and only holds the first element
class LinkList
{
	protected:
		Node* first;
		int mySize;

	public:
		LinkList();
		~LinkList();

		void printLinkList(ostream&);
		void insert(int data, int pos);	//adds an element to the list
		void push(int data);	//adds an element at the end of the list

		void remove(int pos);	//removes an element depending on position
		void pop();				//removes the last element
		bool isEmpty();
		void Erradicate(); //makes the list an "Empty list"

		void sort();	//selection sort in ascending order

		bool operator==(LinkList&);
		void operator=(LinkList);

		//stride takes an existing list and returns it with the same
		//elements with a different order
		LinkList stride(int);
		LinkList elimOdds();
		LinkList mode();

	
};

ostream & operator<<(ostream& out, LinkList&);

#endif
