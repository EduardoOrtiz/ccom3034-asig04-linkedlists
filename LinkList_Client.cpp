/*=============================================================================
Name: 		Eduardo Ortiz
e-mail:		eduardo3991@hotmail.com
=============================================================================*/

#include "LinkList.h"
#include <iostream>
#include <cassert>

using namespace std;

void testinsertAndpush();
void testremove_pop_and_Erradicate();
void testsort();
void teststride();
void testelimOdds();
void testmode();

int main()
{
	testinsertAndpush();

	testremove_pop_and_Erradicate();

	testsort();

	teststride();

    testelimOdds();

	testmode();

	return 0;
}

void testinsertAndpush()
{
	LinkList L, R;

	int A[] = {1, 2, 3, 4, 5, 6};

	for(int i = 0; i < 6; i++)
		L.push(A[i]);

	for(int i = 0; i < 1; i++)
		R.push(i + 1);

	for(int i = 2; i < 6; i++)
		R.push(i + 1);

	R.insert(2, 1);	//insert 2, on the first position(starting from 0)

	assert(L == R);

	cout << "Passed insert and push!" << endl;
}

void testremove_pop_and_Erradicate()
{
	LinkList L, R, S, Empty;

	int A[] = {1, 2, 3, 4, 5, 6};

	for(int i = 0; i < 6; i++)
		L.push(A[i]);

	for(int i = 0; i < 5; i++)
		R.push(A[i]);

	L.pop();

	assert(L == R);

	cout << "Passed pop!" << endl;

	for(int c = 1; c < 5; c++)
		S.push(A[c]);

	L.remove(0);

	assert(L == S);

	cout << "Passed remove!" << endl;

	L.Erradicate();

	assert(L == Empty);

	cout << "Passed Erradicate!" << endl;
}

void testsort()
{
	LinkList L, R;

	int A[] = {1, 2, 3, 4, 5, 6};

	for(int c = 0; c < 6; c++)
	{
		L.push(A[c]);
		R.push(A[6 - (c + 1)]);
	}

	R.sort();

	assert(L == R);

	cout << "Paased sort!" << endl;
}

void teststride()
{
	LinkList LLL, S2, S3, S4, S6, Empty;

	int Original[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
	int Stride_2[] = {1, 3, 5, 7, 9, 11, 2, 4, 6, 8, 10, 12};
	int Stride_3[] = {1, 4, 7, 10, 2, 5, 8, 11, 3, 6, 9, 12};
	int Stride_4[] = {1, 5, 9, 2, 6, 10, 3, 7, 11, 4, 8, 12};
	int Stride_6[] = {1, 7, 2, 8, 3, 9, 4, 10, 5, 11, 6, 12};

	for(int c = 0; c < 12; c++)
	{
		LLL.push(Original[c]);
		S2.push(Stride_2[c]);
		S3.push(Stride_3[c]);
		S4.push(Stride_4[c]);
		S6.push(Stride_6[c]);
	}

	assert(LLL.stride(1) == LLL);
	assert(LLL.stride(2) == S2);
	assert(LLL.stride(3) == S3);
	assert(LLL.stride(4) == S4);
	assert(LLL.stride(6) == S6);
	assert(LLL.stride(7) == Empty);
	assert(LLL.stride(12) == LLL);

	cout << "Passed stride!" << endl;
}



void testelimOdds()
{
	LinkList L, R;

	int A[] = {1, 2, 3, 4, 5, 6};

	for(int c = 0; c < 6; c++)
		L.push(A[c]);

	R.push(2);
	R.push(4);
	R.push(6);

	assert(L.elimOdds() == R);

	cout << "Passed elimOdds!" << endl;
}

void testmode()
{
	LinkList Empty, Practice_Mode1, Practice_Mode2, Practice_Mode3;
	LinkList Practice_Mode4;
	LinkList Mode1, Mode3, Test;

	int A1[] = {1, 3, 4, 6, 8, 5, 3};
	int A2[] = {1, 3, 4, 6, 8, 5, 7};
	int A3[] = {1, 3, 3, 5, 8, 5, 7};
	int A4[] = {1, 1, 3, 5, 3, 5};

	for(int c = 0; c < 7; c++)
	{
		Practice_Mode1.push(A1[c]);
		Practice_Mode2.push(A2[c]);
		Practice_Mode3.push(A3[c]);
	}

	for(int c = 0; c < 6; c++)
		Practice_Mode4.push(A4[c]);

	Mode1.push(3);

	Mode3.push(3);
	Mode3.push(5);

	Test = Practice_Mode1.mode();

	//cout << Test << endl;

	assert(Practice_Mode1.mode() == Mode1);
	assert(Practice_Mode2.mode() == Empty);
	assert(Practice_Mode3.mode() == Mode3);
	assert(Practice_Mode4.mode() == Empty);

	cout << "Passed mode!" << endl;
}
