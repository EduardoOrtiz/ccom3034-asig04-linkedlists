/*=============================================================================
Name: 		Eduardo Ortiz
e-mail:		eduardo3991@hotmail.com
=============================================================================*/

#include "LinkList.h"

//Default Constructor
//Makes the List's first element point to NULL
LinkList::LinkList()
{
	first = NULL;
	mySize = 0;
}

//Destructor
//Frees up memory using the remove function
LinkList::~LinkList()
{
	while(!isEmpty())
		remove(0);
		
	first = NULL;
}

//Function to display the list to the user
void LinkList::printLinkList(ostream& out)
{
	if(this->isEmpty())
	{
		out << "Empty List";
		return;
	}

	Node* Nodeptr = first;

	while(Nodeptr != NULL)
	{
		out << Nodeptr->element << " ";
		Nodeptr = Nodeptr->next;
	}
}

//Calls the printLinkList to display
ostream & operator<<(ostream& out, LinkList& R)
{
	R.printLinkList(out);
	return out;
}

void LinkList::insert(int data, int pos)
{
	//out of bounds
	if(pos < 0 or pos > mySize) return;

	//The name is self explanatory
	Node* Inserting = new Node(data);

	//Case for First
	if(pos == 0)
	{
		Inserting->next = first;	//this element points to NULL
		first = Inserting;			//first points to the new Node
	}

	else
	{
		Node* Nodeptr = first;

		//Go until the Node that is pointing to the desired Node
		for(int c = 0; c < (pos - 1); c++)
			Nodeptr = Nodeptr->next;

		Inserting->next = Nodeptr->next;
		Nodeptr->next = Inserting;
	}

	mySize++;	//new element is added
}

void LinkList::push(int data)
{
	this->insert(data, mySize);
}

void LinkList::remove(int pos)
{
	//out of bounds
	if(pos < 0 or pos >= mySize) return;

	Node* Bridge = first; //the savior
	Node* Killer = first;	//the killer

	if(pos == 0)
	{
		first = first->next;
		delete Killer;
	}

	else
	{
		//looks for the Node that is before
		for(int c = 0; c < (pos - 1); c++)
			Bridge = Bridge->next;

		//the Killer is pointing to the object of importance
		Killer = Bridge->next;

		//The Bridge(previos Node) is now pointing to the next Node
		Bridge->next = Killer->next;

		//i cry every tiem ;_;
		delete Killer;
	}

	mySize--;
}

void LinkList::pop()
{
	this->remove(mySize - 1);
}

bool LinkList::isEmpty()
{
	return (mySize == 0)? true : false;
}

//DEstroys a list and makes it "Empty"
void LinkList::Erradicate()
{
	while(!(this->isEmpty()))
		remove(0);

	first = NULL;
	mySize = 0;
}

//Implementation of Selection Sort with LinkedLists
void LinkList::sort()
{
	if(this->isEmpty() or mySize == 1) return;

	Node* minimum = first;		//will point to the minimum
	Node* Starting = first;		//starting position
	Node* Nodeptr = first;		//pivot of sorts
	int swapper;				//to swap stuff

	//Usual loop to traverse the entire list, comparing the starting element
	//with the others
	while(Starting != NULL)
	{
		minimum = Starting;
		swapper = Starting->element;
		Nodeptr = Starting ->next;

		while(Nodeptr != NULL)
		{
			if(minimum->element > Nodeptr->element)
				minimum = Nodeptr;

			Nodeptr = Nodeptr->next;
		}

		Starting->element = minimum->element;
		minimum->element = swapper;

		Starting = Starting->next;
	}
}

bool LinkList::operator==(LinkList& R)
{
	if(mySize != R.mySize) return false;

	Node* LeftNode = first;
	Node* RightNode = R.first;

	while(LeftNode != NULL)
	{
		if(LeftNode->element != RightNode->element)
			return false;

		LeftNode = LeftNode->next;
		RightNode = RightNode->next;
	}

	return true;
}

void LinkList::operator=(LinkList R)
{
	this->Erradicate();

	//LinkList L;

	Node* Nodeptr = R.first;

	while(Nodeptr != NULL)
	{
		this->push(Nodeptr->element);
		//L.push(Nodeptr->element);
		Nodeptr = Nodeptr->next;
	}

	//return *this;
}

//Stride function
//Takes the existing list and returns a list with the same elements
//with a different order depending on its divisor
//Example:
//L = (1, 2, 3, 4, 5, 6)
//L.stride(2) = (1, 3, 5, 2, 4, 6)
//L.stride(5) = () **empty**
LinkList LinkList::stride(int divisor)
{
	LinkList R;
	int counter = 0;	//variable used to traverse the list
	Node* Nodeptr = first;

    if(this->isEmpty()) return R;	//nothing to do if empty
	
	//In case the argument isn't a divisor, or it's negative**
	//**even thou the negative number divides it just as well
	if((mySize % divisor != 0) or (divisor < 1)) return R;

	//Loop to traverse the entire list
	for(int m = 0; m < mySize; m++)
	{
		Nodeptr = first;

		//the case for the last element
		if(m == (mySize - 1))
		{
			for(int c = 0; c < m; c++)
				Nodeptr = Nodeptr->next;

			R.push(Nodeptr->element);

			break;
		}

		//to make sure one doesn't go out of bounds
		if(counter >= mySize)
			counter -= (mySize -1);

		for(int n = 0; n < counter; n++)
			Nodeptr = Nodeptr->next;

		R.push(Nodeptr->element);

		counter += divisor;
	}
	return R;
}

//Eliminates all the Odds, I guess
LinkList LinkList::elimOdds()
{
	LinkList R;

	Node* Nodeptr = first;

	while(Nodeptr != NULL)
	{
		if(Nodeptr->element % 2 == 0)
			R.push(Nodeptr->element);

		Nodeptr = Nodeptr->next;
	}

	return R;
}

//Mode function
//Returns a list with the mode of the original list
//Example:
//L = (1, 1, 2, 3, 4, 4)
//L.mode() = (1, 4)
LinkList LinkList::mode()
{
	LinkList R;

	//parallel array that will keep count of the amount of elements
	SweetArray AmountArray(mySize);	
	
	//parallel array that will keep the Unique elements
	SweetArray UniqueElements(mySize);

	int amountMax, uniqueSize, position = 0;

	Node* Nodeptr = first;
	Node* Pivot = Nodeptr->next;

	while(Nodeptr != NULL)
	{
		AmountArray[position] = 1;	//assumes every element appears once
		UniqueElements[position++] = Nodeptr->element;
		Nodeptr = Nodeptr->next;
	}

	Nodeptr = first;
	position = 0;

	//Loop that will compare a starting element with the rest.
	//If it encounters itself, AmountArray will increase the value
	//on its position
	while(Nodeptr != NULL)
	{
		Pivot = Nodeptr->next;

		while(Pivot != NULL)
		{
			if((Nodeptr->element) == (Pivot->element))
				AmountArray[position]++;

			Pivot = Pivot->next;
		}

		Nodeptr = Nodeptr->next;
		position++;
	}

	UniqueElements.removeDuplicates();	//the array is now unique
	uniqueSize = UniqueElements.getSize();

	amountMax = AmountArray.getMax();

	if((amountMax == mySize) or (amountMax == 1)) return R;

	if(uniqueSize * amountMax == mySize) return R;

	Nodeptr = first;

	//loop that will put the mode elements in the returning list
	for(int c = 0; c < mySize; c++)
	{
		if(amountMax == AmountArray[c])
			R.push(Nodeptr->element);

		Nodeptr = Nodeptr->next;
	}

	return R;
}



